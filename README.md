# About this project

This project is part of the Technical Test for Cuban Engineer Team. It was made using React + Bootstrap. This is just the frontend of the application... in the future I'm planning to add a backend to persist the information added

### Installation

- Clone or download the repo
- Move to the root folder and run `npm install` command in a terminal
- Once it finishes installing the dependencies, you can run `npm start`, which should open a browser with the app

### For developers

I assume there are a lot of things that could be optimized, so if you want to share some knowledge, feel free to contribute or email me to: alekosmp86@gmail.com

BE AWARE: There is a little "hack" in the task's editor... at the moment of implementing it, I didn't know how to transform the input's text into a more stylish text (I don't think the HTML input component allows it). So what I did was to create a `<p>` element, and inside I added `<span>` tags for those words that uses tags such as @, #, links or emails. I think there is a more elegant way to make this using a rich text editor, but it will have to wait for me to have time available to test it properly

Also, there is a non-stop (somewhat heated) discussion in forums about the regular expression used for links. The one I use is by no means perfect, so you might find cases where the highlighted text is not a valid url (so sorry about that, please show me one that works well)

### Final thoughts

This was actually a very interesting project, it pushed me out of my comfort zone and I learned new things about React while getting a better grasp of previous learned things.
Shout out to Cuban Engineer Team.
