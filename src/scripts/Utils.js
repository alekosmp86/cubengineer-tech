export const stylizeText = (text, smallScreenMode) => {
  const words = text.split(" ");
  const transformed = [];
  let mailCounter = 0,
    linkCounter = 0;

  words.forEach((word) => {
    let icon;
    let color = word.startsWith("#")
      ? "danger"
      : word.startsWith("@")
      ? "success"
      : validateEmail(word)
      ? "warning"
      : validateURL(word)
      ? "info"
      : "";
    const wordHasTag = color !== "";
    const originalWord = word;

    if (!wordHasTag) {
      transformed.push(`${originalWord} `);
      return;
    }

    switch (color) {
      case "warning": {
        icon = <span className="bi bi-envelope"></span>;
        mailCounter++;
        word = smallScreenMode ? `Mail ${mailCounter}` : originalWord;
        break;
      }
      case "info": {
        icon = <span className="bi bi-link-45deg"></span>;
        linkCounter++;
        word = `Link ${linkCounter}`;
        break;
      }
      default: {
        break;
      }
    }

    transformed.push(
      <span
        key={`word_${Math.random()}`}
        role={wordHasTag ? "button" : "none"}
        title={wordHasTag ? originalWord : ""}
        className={`${
          wordHasTag ? `badge rounded-pill bg-${color}` : "text-white"
        }`}
      >
        {icon} {`${word} `}
      </span>
    );
  });

  return transformed;
};

const IdGenerator = function* () {
  let id = Date.now();
  while (true) {
    yield id;
    id++;
  }
};

export const UUID = IdGenerator();

const validateEmail = (text) => {
  const emailPattern =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailPattern.test(text);
};

const validateURL = (text) => {
  const urlPattern =
    /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/;
  return urlPattern.test(text);
};
