const MutableButton = (props) => {
  const handleClickAction = () => {
    props.mutated && props.mutatedAction();
    !props.mutated && props.defaultAction();
  };

  const getCurrentIcon = () => {
    if (props.mutated) {
      return props.icons.mutated;
    }

    return props.icons.default;
  };

  return (
    <button
      data-testid="mutableButton"
      type="button"
      className={`btn text-white me-2 ${props.cssClasses}`}
      onClick={handleClickAction}
    >
      {props.icons && (
        <span
          data-testid="mutableIcon"
          className={`bi bi-${getCurrentIcon()}`}
        ></span>
      )}
    </button>
  );
};

export default MutableButton;
