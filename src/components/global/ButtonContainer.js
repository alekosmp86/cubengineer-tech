const ButtonContainer = ({ children }) => {
  return (
    <div className="d-flex flex-row justify-content-between mt-2 mb-2 ms-2 me-2">
      {children}
    </div>
  );
};

export default ButtonContainer;
