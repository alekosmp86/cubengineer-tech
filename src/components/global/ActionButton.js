const ActionButton = (props) => {
  return (
    <button
      data-testid="actionButton"
      type="button"
      className={`btn text-white me-2 ${props.cssClasses}`}
      style={props.customStyle}
      disabled={props.isDisabled}
      onClick={props.action}
    >
      {props.icon && (
        <span
          data-testid="buttonIcon"
          className={`bi bi-${props.icon} ${!props.hideLabel && "me-2"}`}
        ></span>
      )}
      {!props.hideLabel && props.label}
    </button>
  );
};

export default ActionButton;
