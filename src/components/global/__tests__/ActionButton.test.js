import React from "react";
import { render, cleanup } from "@testing-library/react";
import ActionButton from "../ActionButton";

afterEach(cleanup);

it("Button is rendered with label", () => {
  const buttonText = "Ok";
  const { getByTestId } = render(<ActionButton label={buttonText} />);
  expect(getByTestId("actionButton")).toHaveTextContent(buttonText);
});

it("Button is rendered without label", () => {
  const { getByTestId } = render(<ActionButton />);
  expect(getByTestId("actionButton")).toHaveTextContent("");
});

it("Button is rendered with icon", () => {
  const className = "bi-arrows-angle-expand";
  const { getByTestId } = render(<ActionButton icon="arrows-angle-expand" />);
  expect(getByTestId("buttonIcon")).toHaveClass(className);
});

it("Button is disabled", () => {
  const { getByTestId } = render(<ActionButton isDisabled={true} />);
  expect(getByTestId("actionButton")).toBeDisabled();
});
