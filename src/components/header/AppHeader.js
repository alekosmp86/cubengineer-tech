import styles from "./AppHeader.module.css";

const AppHeader = ({ title }) => {
  return (
    <div
      className={`bg-dark ${styles.header} d-flex flex-row justify-content-center align-items-center`}
    >
      <h2 data-testid="appHeaderTitle" className="text-white">
        {title}
      </h2>
    </div>
  );
};

export default AppHeader;
