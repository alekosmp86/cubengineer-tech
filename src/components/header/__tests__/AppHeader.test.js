import React from "react";
import AppHeader from "./../AppHeader";
import { render, cleanup } from "@testing-library/react";

afterEach(cleanup);

it("App header component renders correctly", () => {
  const displayedText = "App header title";
  const { getByTestId } = render(<AppHeader title={displayedText} />);
  expect(getByTestId("appHeaderTitle")).toHaveTextContent(displayedText);
});
