import styles from "./TaskInput.module.css";

const TaskInput = ({ onFocus, onChange, collapsed, taskText }) => {
  const handleInputFocus = () => {
    onFocus?.();
  };

  const handleTaskInputChange = (e) => {
    onChange?.(e.target.value);
  };

  return (
    <>
      <div className="d-flex flex-row align-items-center ms-2 mt-2 me-2 mb-2">
        <span
          className={`bi bi-plus d-flex flex-column justify-content-center align-items-center text-white
                  border border-primary border-3 rounded ${styles.buttonPlus}`}
        ></span>
        <input
          type="text"
          className="form-control ms-2"
          placeholder="Type to add new task"
          value={taskText}
          onClick={handleInputFocus}
          onChange={handleTaskInputChange}
        />
        {!collapsed && (
          <img
            src="images/avatar.png"
            alt="avatar"
            className="avatar ms-2"
          ></img>
        )}
      </div>
    </>
  );
};

export default TaskInput;
