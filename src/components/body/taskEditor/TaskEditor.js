import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addTask } from "../../../redux/actions/taskActions";
import { stylizeText } from "../../../scripts/Utils";
import { useMediaQuery } from "react-responsive";
import TaskInput from "../taskInput/TaskInput";

const TaskEditor = ({ children }) => {
  const smallScreenSize = useMediaQuery({ query: `(max-width: 1230px)` });
  const [collapsed, setCollapsed] = useState(true);
  const [taskText, setTaskText] = useState("");
  const [styledText, setStyledText] = useState([]);
  const [toolButtonDisabled, setToolButtonDisabled] = useState(true);

  const dispatch = useDispatch();

  useEffect(() => {
    const identifier = setTimeout(() => {
      setStyledText(stylizeText(taskText, smallScreenSize));
    }, 250);

    return () => {
      clearTimeout(identifier);
    };
  });

  const handleCollapse = () => {
    if (collapsed) {
      setCollapsed((prevCollapsedValue) => {
        return !prevCollapsedValue;
      });
    }
  };

  const handleTaskInputChange = (inputValue) => {
    if (inputValue !== "") {
      toolButtonDisabled && setToolButtonDisabled(false);
    } else {
      setToolButtonDisabled(true);
    }

    setTaskText(inputValue);
  };

  const handleTaskAdded = () => {
    if (taskText !== "") {
      //add a new task
      dispatch(addTask(taskText));
    }
    clearEditor();
  };

  const handleTaskCancelled = () => {
    clearEditor();
  };

  const clearEditor = () => {
    setCollapsed(true);
    setToolButtonDisabled(true);
    setTaskText("");
  };

  return (
    <div className="bg-dark border border-1 border-secondary rounded">
      <TaskInput
        onFocus={handleCollapse}
        onChange={handleTaskInputChange}
        collapsed={collapsed}
        taskText={taskText}
      />
      {!collapsed && (
        <div className="ms-2 me-2">
          <div className="text-secondary smallText">Task message preview:</div>
          <p className="text-secondary smallText">{styledText}</p>
        </div>
      )}
      {!collapsed &&
        React.Children.map(children, (child) => {
          return React.cloneElement(child, {
            buttonDisabled: toolButtonDisabled,
            onSaveTask: handleTaskAdded,
            onCancelTask: handleTaskCancelled,
          });
        })}
    </div>
  );
};

export default TaskEditor;
