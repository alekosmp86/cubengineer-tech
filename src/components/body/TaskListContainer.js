import { useSelector } from "react-redux";
import TaskEditor from "./taskEditor/TaskEditor";
import TaskListElement from "./taskListElement/TaskListElement";
import NewTaskToolbar from "./taskEditorToolbar/NewTaskToolbar";
import { UUID } from "../../scripts/Utils";

const TaskListContainer = () => {
  const tasks = useSelector((store) => store.task);

  const hasTasks = () => {
    return tasks.length > 0;
  };

  return (
    <>
      <div className="d-flex flex-column ms-2 mt-2 me-2">
        <TaskEditor>
          <NewTaskToolbar />
        </TaskEditor>
      </div>
      <div
        className={`d-flex flex-column bg-dark ms-2 mt-2 me-2 h-50 mh-50 border border-secondary border-1 rounded overflow-auto ${
          !hasTasks() && "justify-content-center align-items-center"
        }`}
      >
        {!hasTasks() && (
          <div className="">
            <h2 className="text-secondary">No tasks to show</h2>
          </div>
        )}
        {tasks.map((task) => {
          return <TaskListElement key={UUID.next().value} task={task} />;
        })}
      </div>
    </>
  );
};

export default TaskListContainer;
