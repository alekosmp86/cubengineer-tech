import ActionButton from "../../global/ActionButton";
import ButtonContainer from "../../global/ButtonContainer";
import { useMediaQuery } from "react-responsive";
import MutableButton from "../../global/MutableButton";

const NewTaskToolbar = ({ buttonDisabled, onSaveTask, onCancelTask }) => {
  const smallScreenSize = useMediaQuery({
    query: `(max-width: 1230px)`,
    noSsr: true,
  });

  const handleAddButtonClick = () => {
    onSaveTask?.();
  };

  const handleCancelButtonClick = () => {
    onCancelTask?.();
  };

  return (
    <ButtonContainer>
      <div className="d-flex flex-row">
        <div>
          <ActionButton
            label="Open"
            hideLabel={smallScreenSize}
            icon="arrows-angle-expand"
            cssClasses="btn-secondary"
            isDisabled={buttonDisabled}
          />
        </div>
        <div className="ms-4">
          <ActionButton
            label="Today"
            hideLabel={smallScreenSize}
            icon="calendar"
            cssClasses="btn-secondary"
            isDisabled={buttonDisabled}
          />
          <ActionButton
            label="Public"
            hideLabel={smallScreenSize}
            icon="unlock"
            cssClasses="btn-secondary"
            isDisabled={buttonDisabled}
          />
          <ActionButton
            label="Normal"
            hideLabel={smallScreenSize}
            icon="asterisk"
            cssClasses="btn-secondary"
            isDisabled={buttonDisabled}
          />
          <ActionButton
            label="Estimation"
            hideLabel={smallScreenSize}
            icon="calendar-date"
            cssClasses="btn-secondary"
            isDisabled={buttonDisabled}
          />
        </div>
      </div>
      {!smallScreenSize && (
        <div>
          <ActionButton
            label="Cancel"
            cssClasses="btn-secondary"
            action={handleCancelButtonClick}
          />
          <ActionButton
            label={buttonDisabled ? "Ok" : "Add"}
            cssClasses="btn-primary"
            action={handleAddButtonClick}
          />
        </div>
      )}
      {smallScreenSize && (
        <div>
          <MutableButton
            cssClasses="btn-primary"
            mutated={!buttonDisabled}
            icons={{ default: "x-lg", mutated: "file-plus" }}
            defaultAction={handleCancelButtonClick}
            mutatedAction={handleAddButtonClick}
          />
        </div>
      )}
    </ButtonContainer>
  );
};

export default NewTaskToolbar;
