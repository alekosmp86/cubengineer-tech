import ActionButton from "../../global/ActionButton";
import ButtonContainer from "../../global/ButtonContainer";
import { useMediaQuery } from "react-responsive";

const EditTaskToolbar = ({ buttonDisabled, onSaveTask, onCancelTask }) => {
  const smallScreenSize = useMediaQuery({
    query: `(max-width: 1230px)`,
    noSsr: true,
  });

  return (
    <ButtonContainer>
      <div className="d-flex flex-row">
        <div>
          <ActionButton
            label="Open"
            hideLabel={smallScreenSize}
            icon="arrows-angle-expand"
            cssClasses="btn-dark"
            isDisabled={buttonDisabled}
          />
        </div>
        <div>
          <ActionButton
            label="Today"
            hideLabel={smallScreenSize}
            icon="calendar"
            cssClasses="btn-dark"
            isDisabled={buttonDisabled}
          />
          <ActionButton
            label="Public"
            hideLabel={smallScreenSize}
            icon="unlock"
            cssClasses="btn-dark"
            isDisabled={buttonDisabled}
          />
          <ActionButton
            label="Normal"
            hideLabel={smallScreenSize}
            icon="asterisk"
            cssClasses="btn-dark"
            isDisabled={buttonDisabled}
          />
          <ActionButton
            label="Estimation"
            hideLabel={smallScreenSize}
            icon="calendar-date"
            cssClasses="btn-dark"
            isDisabled={buttonDisabled}
          />
          <ActionButton
            label="Delete"
            hideLabel={smallScreenSize}
            icon="trash"
            cssClasses="btn-dark"
            isDisabled={buttonDisabled}
          />
        </div>
      </div>
    </ButtonContainer>
  );
};

export default EditTaskToolbar;
