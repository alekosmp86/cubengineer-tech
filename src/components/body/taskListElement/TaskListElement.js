import EditTaskToolbar from "../taskEditorToolbar/EditTaskToolbar";
import Task from "./Task";

const TaskListElement = ({ task }) => {
  return (
    <div className="bg-secondary border border-1 border-light rounded ms-2 me-2 mt-2">
      <Task text={task} />
      <EditTaskToolbar />
    </div>
  );
};

export default TaskListElement;
