import { useMediaQuery } from "react-responsive";
import { stylizeText } from "../../../scripts/Utils";

const Task = ({ text }) => {
  const smallScreenSize = useMediaQuery({ query: `(max-width: 1230px)` });

  const getCurrentDateTime = () => {
    const date = new Date();
    let fullDateTime = `${date.toLocaleDateString()} at ${date.toLocaleTimeString()}`;
    return fullDateTime;
  };

  return (
    <div className="ms-2 me-2 mt-2 rounded bg-secondary">
      <span className="text-warning smallText fw-bold">
        Created: {getCurrentDateTime()}
      </span>
      <div className="ms-2 mt-2 me-2 mb-2 d-flex flex-row align-items-center justify-content-between">
        <div className="d-flex flex-row">
          <input
            className="form-check-input"
            type="checkbox"
            value=""
            id="flexCheckDefault"
          />
          {smallScreenSize ? (
            <div className="ms-2">{stylizeText(text, true)}</div>
          ) : (
            <div className="ms-2">{stylizeText(text, false)}</div>
          )}
        </div>
        <div>
          <img
            src="images/avatar.png"
            alt="avatar"
            className="avatar ms-2"
          ></img>
        </div>
      </div>
    </div>
  );
};

export default Task;
