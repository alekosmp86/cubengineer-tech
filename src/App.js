import TaskListContainer from "./components/body/TaskListContainer";
import AppHeader from "./components/header/AppHeader";

function App() {
  return (
    <div className="vh-100 bg-secondary">
      <AppHeader title="Tasks Manager" />
      <TaskListContainer />
    </div>
  );
}

export default App;
